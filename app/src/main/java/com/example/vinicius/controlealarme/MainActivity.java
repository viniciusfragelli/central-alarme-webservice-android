package com.example.vinicius.controlealarme;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView texto_status;
    private TextView texto_but;
    private LinearLayout but;
    private LinearLayout but_panico;
    private TextView texto_panico;
    private boolean status_alarme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto_status = (TextView) findViewById(R.id.txt_status);
        texto_but = (TextView) findViewById(R.id.txt_but);
        but = (LinearLayout) findViewById(R.id.but);
        texto_panico = (TextView) findViewById(R.id.txt_but_panico);
        but_panico = (LinearLayout) findViewById(R.id.but_panico);
        but_panico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPanico();
            }
        });
        setEnabledButtons(false);
        new VerificaConexao().execute();
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlarme();
            }
        });
    }

    private void onClickPanico() {
        System.out.println("Panico");
        but_panico.removeAllViews();
        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleSmall);
        but_panico.addView(progressBar);
        new SetPanico().execute();
        setEnabledButtons(false);
    }

    private void setEnabledButtons(boolean status){
        but.setEnabled(status);
        but_panico.setEnabled(status);
    }

    public void onClickAlarme() {
        System.out.println("Executou");
        but.removeAllViews();
        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleSmall);
        but.addView(progressBar);
        new SetStatusAlarme().execute();
        setEnabledButtons(false);
    }

    public void onClickReload(View view) {
        new VerificaConexao().execute();
        texto_status.setText("Conectando...");
    }

    private class VerificaConexao extends AsyncTask<Void,Void,Void>{

        private JSONObject ob;

        @Override
        protected Void doInBackground(Void... params) {
            ob = null;
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(URLs.STATUS);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(5000);
                urlConnection.setReadTimeout(5000);
                urlConnection.connect();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    Log.e("Status",sb.toString());
                    ob = new JSONObject(sb.toString());
                }else{
                    System.out.println("Vish  deu merda");
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(ob != null){
                try {
                    if(ob.getInt("alarme") == 0){
                        texto_status.setText("Desligado");
                        texto_but.setText("Ligar alarme");
                        status_alarme = false;
                        texto_status.setTextColor(Color.WHITE);
                    }else{
                        if(ob.getInt("disparado") == 0){
                            texto_status.setText("Ligado");
                            texto_but.setText("Desligar alarme");
                            texto_status.setTextColor(Color.GREEN);
                            status_alarme = true;
                        }else{
                            texto_status.setText("DISPARADO");
                            status_alarme = true;
                            texto_but.setText("Desligar alarme");
                            texto_status.setTextColor(Color.RED);
                        }
                    }
                    setEnabledButtons(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    texto_status.setText("Erro na leitura");
                    texto_status.setTextColor(Color.RED);
                }
            }else{
                texto_status.setText("Conectando...");
                texto_status.setTextColor(Color.WHITE);
            }
        }
    }

    private class SetStatusAlarme extends AsyncTask<Boolean,Boolean,Boolean>{

        @Override
        protected Boolean doInBackground(Boolean... params) {
            URL url;
            HttpURLConnection urlConnection = null;
            boolean res = false;
            try {
                if(status_alarme)
                    url = new URL(URLs.DESLIGAR);
                else
                    url = new URL(URLs.LIGAR);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(5000);
                urlConnection.setReadTimeout(5000);
                urlConnection.connect();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    res = true;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(urlConnection != null)urlConnection.disconnect();
            }
            return res;
        }

        @Override
        protected void onPostExecute(Boolean res) {
            super.onPostExecute(res);
            if(res){
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("Status do alarme alterado!").setPositiveButton("OK",null).show();
                but.removeAllViews();
                but.addView(texto_but);
                new VerificaConexao().execute();
            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("Não foi possível concluir sua solicitação!").setPositiveButton("OK",null).show();
            }
        }
    }

    private class SetPanico extends AsyncTask<Boolean,Boolean,Boolean>{

        @Override
        protected Boolean doInBackground(Boolean... params) {
            URL url;
            HttpURLConnection urlConnection = null;
            boolean res = false;
            try {
                url = new URL(URLs.PANICO);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(5000);
                urlConnection.setReadTimeout(5000);
                urlConnection.connect();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    res = true;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(urlConnection != null)urlConnection.disconnect();
            }
            return res;
        }

        @Override
        protected void onPostExecute(Boolean res) {
            super.onPostExecute(res);
            if(res){
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("Pânico acionado!").setPositiveButton("OK",null).show();
                but_panico.removeAllViews();
                but_panico.addView(texto_panico);
                new VerificaConexao().execute();
            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("Não foi possível concluir sua solicitação!").setPositiveButton("OK",null).show();
            }
        }
    }

}
